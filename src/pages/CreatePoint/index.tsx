import React, { useState, useEffect, ChangeEvent, FormEvent } from "react";
import "./styles.css";
import { FiArrowLeft } from "react-icons/fi";
import { Link, useHistory } from "react-router-dom";
import logo from "../../assets/logo.svg";

import { Map, TileLayer, Marker } from "react-leaflet";
import api from "../../services/api";
import axios from "axios";
import { LeafletMouseEvent } from "leaflet";
import Dropzone from "../../components/Dropzone";

interface Item {
	id: number;
	title: string;
	image_url: string;
}

interface IBGE {
	sigla: string;
}

interface City {
	nome: string;
}

const CreatePoint: React.FC = () => {
	const history = useHistory();
	const [items, setItems] = useState<Item[]>([]);
	const [ufs, setUfs] = useState<string[]>([]);
	const [cities, setCities] = useState<string[]>([]);

	const [formData, setFormData] = useState({
		name: "",
		whatsapp: "",
		email: "",
	});

	const [selectedFile, setSelectedFile] = useState<File>();
	const [selectedItems, setSelectedItems] = useState<number[]>([]);
	const [selectedUF, setSelectedUF] = useState("0");
	const [selectedCity, setSelectedCity] = useState("0");
	const [selectedPosition, setSelectedPosition] = useState<[number, number]>([
		0,
		0,
	]);
	const [initialPosition, setInitialPosition] = useState<[number, number]>([
		0,
		0,
	]);

	useEffect(() => {
		navigator.geolocation.getCurrentPosition((res) => {
			setInitialPosition([res.coords.latitude, res.coords.longitude]);
		});
	}, []);

	useEffect(() => {
		api.get("/items").then((res) => {
			setItems(res.data);
		});
	}, []);

	useEffect(() => {
		axios
			.get(
				"https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome"
			)
			.then((res) => {
				const siglas = res.data.map((uf: IBGE) => uf.sigla);
				setUfs(siglas);
			});
	}, []);

	function handleSelectedUF(e: ChangeEvent<HTMLSelectElement>) {
		setSelectedUF(e.target.value);
	}

	useEffect(() => {
		if (selectedUF !== "0") {
			axios
				.get(
					`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUF}/municipios`
				)
				.then((res) => {
					const cidades = res.data.map((city: City) => city.nome);
					setCities(cidades);
				});
		}
	}, [selectedUF]);

	function handleSelectedCity(e: ChangeEvent<HTMLSelectElement>) {
		setSelectedCity(e.target.value);
	}

	function handleMapClick(e: LeafletMouseEvent) {
		setSelectedPosition([e.latlng.lat, e.latlng.lng]);
	}

	function handleInputChange(e: ChangeEvent<HTMLInputElement>) {
		setFormData({
			...formData,
			[e.target.name]: e.target.value,
		});
	}

	function handleSelectItem(id: number) {
		const alreadySelected = selectedItems.findIndex((i) => i === id);

		if (alreadySelected > -1) {
			const filteredItems = selectedItems.filter((i) => i !== id);
			setSelectedItems(filteredItems);
		} else {
			setSelectedItems([...selectedItems, id]);
		}
	}

	async function handleSubmit(e: FormEvent) {
		e.preventDefault();

		const { name, email, whatsapp } = formData;
		const uf = selectedUF;
		const city = selectedCity;
		const [latitude, longitude] = selectedPosition;
		const items = selectedItems;
		const image = selectedFile;

		const data = new FormData();
		data.append("name", name);
		data.append("email", email);
		data.append("whatsapp", whatsapp);
		data.append("uf", uf);
		data.append("city", city);
		data.append("latitude", String(latitude));
		data.append("longitude", String(longitude));
		data.append("items", items.join(","));

		if (image) {
			data.append("image", image);
		}

		await api.post("/points", data);
		alert("Registro cadastrado com sucesso");

		history.push("/");
	}

	return (
		<div id="page-create-point">
			<header>
				<img src={logo} alt="Ecoleta" />

				<Link to="/">
					<FiArrowLeft />
					Voltar para home
				</Link>
			</header>

			<form onSubmit={handleSubmit}>
				<h1>Cadastro do ponto de coleta</h1>

				<Dropzone onFileUpload={setSelectedFile} />

				<fieldset>
					<legend>
						<h2>Dados</h2>
					</legend>

					<div className="field">
						<label htmlFor="name">Nome da entidade</label>
						<input
							type="text"
							name="name"
							id="name"
							onChange={handleInputChange}
						/>
					</div>

					<div className="field-group">
						<div className="field">
							<label htmlFor="email">E-mail</label>
							<input
								type="email"
								name="email"
								id="email"
								onChange={handleInputChange}
							/>
						</div>
						<div className="field">
							<label htmlFor="whatsapp">Whatsapp</label>
							<input
								type="text"
								name="whatsapp"
								id="whatsapp"
								onChange={handleInputChange}
							/>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>
						<h2>Endereco</h2>
						<span>Selecione o endereco no mapa</span>
					</legend>

					<Map center={initialPosition} zoom={14} onClick={handleMapClick}>
						<TileLayer
							attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
							url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
						/>

						<Marker position={selectedPosition} />
					</Map>

					<div className="field-group">
						<div className="field">
							<label htmlFor="uf">Estado (UF)</label>
							<select
								name="uf"
								id="uf"
								value={selectedUF}
								onChange={handleSelectedUF}
							>
								<option value="0">Selecione uma UF</option>
								{ufs.map((i) => (
									<option key={i} value={i}>
										{i}
									</option>
								))}
							</select>
						</div>

						<div className="field">
							<label htmlFor="uf">Cidade</label>
							<select
								name="city"
								id="city"
								value={selectedCity}
								onChange={handleSelectedCity}
							>
								<option value="0">Selecione uma Cidade</option>
								{cities.map((i) => (
									<option key={i} value={i}>
										{i}
									</option>
								))}
							</select>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>
						<h2>Itens de coleta</h2>
						<span>Selecione um ou mais itens abaixo</span>
					</legend>

					<ul className="items-grid">
						{items.map((i) => (
							<li
								key={i.id}
								onClick={() => handleSelectItem(i.id)}
								className={selectedItems.includes(i.id) ? "selected" : ""}
							>
								<img src={i.image_url} alt={i.title} />
								<span>{i.title}</span>
							</li>
						))}
					</ul>
				</fieldset>

				<button type="submit">Cadastrar ponto de coleta</button>
			</form>
		</div>
	);
};

export default CreatePoint;
